#!/usr/bin/env ruby

require 'socket'
require 'io/console'

UP_ARROW = "\e[A"
DOWN_ARROW = "\e[B"
RIGHT_ARROW = "\e[C"
LEFT_ARROW = "\e[D"
CONTROL_C = "\u0003"
 
# Reads keypresses from the user including 2 and 3 escape character sequences.
def read_char
	STDIN.echo = false
	STDIN.raw!

	input = STDIN.getc.chr
	if input == "\e" then
		input << STDIN.read_nonblock(3) rescue nil
		input << STDIN.read_nonblock(2) rescue nil
	end
ensure
	STDIN.echo = true
	STDIN.cooked!

	return input
end

server = TCPServer.new(10157)
loop do
	client = server.accept
	puts "Client #{client} connected"
	until client.closed?
		c = read_char
		case c
		when UP_ARROW
			client.puts "f 1 1000"
		when DOWN_ARROW
			client.puts "b 1 1000"
		when LEFT_ARROW
			client.puts "l 1 200"
		when RIGHT_ARROW
			client.puts "r 1 200"
		when " "
			client.puts "f 1 4000"
		when CONTROL_C
			exit 0
		end
	end
	puts "Client disconnected"
end
